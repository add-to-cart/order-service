package com.domosshop.orderservice;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Profile;

import com.domosshop.orderservice.model.Address;
import com.domosshop.orderservice.model.Cart;
import com.domosshop.orderservice.model.Customer;
import com.domosshop.orderservice.model.ItemCart;
import com.domosshop.orderservice.model.Order;
import com.domosshop.orderservice.model.OrderStat;
import com.domosshop.orderservice.model.OrderStatus;
import com.domosshop.orderservice.model.Payment;
import com.domosshop.orderservice.service.CustomerService;
import com.domosshop.orderservice.service.OrderService;
import com.domosshop.orderservice.service.OrderStatService;

import lombok.extern.log4j.Log4j2;

@SpringBootTest
@Log4j2
@Profile("dev")
@TestMethodOrder(OrderAnnotation.class)
class OrderServiceApplicationTests {

	@Autowired
	OrderService orderService;

	@Autowired
	OrderStatService orderStatService;

	@Autowired
	CustomerService customerService;

	static Customer customerAdded = new Customer();

	static Order orderAdded = new Order();

    @Value(value = "${topic.order.pending}")
    private String pendingOrderTopicName;

	@Test
	void contextLoads() {
	}

	@Test
	@org.junit.jupiter.api.Order(1)
	void testAddCustomer() {
		Payment payment = new Payment("verified", "stripe",
				"credit", 10000, "tok_1LpeoQ2eZvKYlo2CSaq9bVe0");

		Customer customer = new Customer("William", "Ngassa",
				"nguesseuarthur17@gmail.com", null, payment,
				null, null);

		customerAdded = customerService.add(customer);
		log.info(customerAdded);
		customer.setId(customerAdded.getId());
		Assertions.assertEquals(customer, customerAdded);
	}

	@Test
	@org.junit.jupiter.api.Order(2)
	void testAddOrder() {

		ItemCart itemCart = new ItemCart(Long.valueOf(1), 1, 549);
		ArrayList<ItemCart> itemsCart = new ArrayList<>();
		itemsCart.add(itemCart);
		Cart cart = new Cart(itemsCart, LocalDateTime.now());

		Address shipping = new Address("Lille", "Lille Street",
				"Lille Street 2", "70555", "01 43 48 55 48");

		Order order = new Order(customerAdded.getId(), cart, shipping,
				LocalDateTime.now(), OrderStatus.PENDING, 549,
				"Test add order...");
		orderAdded = orderService.addOrder(order);
		log.info(orderAdded);
		order.setId(orderAdded.getId());
		Assertions.assertEquals(order, orderAdded);
	}

	@Test
	@org.junit.jupiter.api.Order(3)
	void testGetAllOrders() {
		List<Order> orders = orderService.getAllOrders();
		Assertions.assertTrue(1 == orders.size());
	}

	@Test
	@org.junit.jupiter.api.Order(4)
	void testUpdateOrder() {
		orderAdded.setStatus(OrderStatus.CONFIRMED);
		Order orderUpdate = orderService.updateOrder(orderAdded);
		Assertions.assertEquals(orderAdded, orderUpdate);
	}

	@Test
	@org.junit.jupiter.api.Order(5)
	void testGetOrdersByCustomerEmail() {
		List<Order> orders = orderService.getOrdersByCustomerEmail(customerAdded.getEmail());
		Assertions.assertTrue(1 == orders.size());
	}

	@Test
	@org.junit.jupiter.api.Order(6)
	void testGetOrderStat() {
		OrderStat orderStat = orderStatService.getOrderStat();
		log.info(orderService.getAllOrders());
		OrderStat orderStat2 = new OrderStat(0, 0, 549, 549, 549, 549);
		Assertions.assertEquals(orderStat, orderStat2);
	}

	@Test
	@org.junit.jupiter.api.Order(7)
	void testSendOrderToKafka() {
		orderService.sendOrderToKafka(orderAdded, pendingOrderTopicName);
	}

	@Test
	@org.junit.jupiter.api.Order(8)
	void testDeleteOrder() {
		orderService.delete(orderAdded);
		Assertions.assertNull(orderService.getOrder(orderAdded.getId()));
	}

	@Test
	@org.junit.jupiter.api.Order(9)
	void testDeleteCustomer() {
		customerService.delete(customerAdded);
		Assertions.assertNull(customerService.get(customerAdded.getId()));
	}
}
