package com.domosshop.orderservice.model;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseModel {
    private LocalDateTime timeStamp = LocalDateTime.now();
    private int statusCode = HttpStatus.BAD_REQUEST.value();
    private String status = HttpStatus.BAD_REQUEST.toString();
    private String reason;
    private String message;
    private String developerMessage;
    private Object data;
    
}
