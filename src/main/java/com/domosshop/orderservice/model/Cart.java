package com.domosshop.orderservice.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cart {
    
    private ArrayList<ItemCart> items;
    private LocalDateTime timestamp;
}
