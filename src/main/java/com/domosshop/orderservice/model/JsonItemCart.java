package com.domosshop.orderservice.model;

import java.util.ArrayList;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JsonItemCart {
    
    @Min(0)
    private Long id;

    @NotBlank
    private String title;
    
    private String description;

    @Min(0)
    private float price;

    @Min(0)
    private float discountPercentage;

    @Min(0)
    @Max(5)
    private float rating;

    @Min(0)
    private int stock;
    private String brand;
    private String category;
    private String thumbnail;
    private ArrayList<String> images;

    @Min(0)
    private int quantity;
    
    @Min(0)
    private float total;
}
