package com.domosshop.orderservice.model;

import java.util.ArrayList;

import javax.validation.Valid;

import lombok.Data;

@Data
public class OrderJson {
    @Valid
    private UserData userData;

    @Valid
    private ArrayList<JsonItemCart> carts;

    @Valid
    private String grandTotal;
}
