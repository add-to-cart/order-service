package com.domosshop.orderservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderStat {
    private int nbrOrderRejected;
    private int nbrOrderCanceled;
    private float receiptByHour;
    private float receiptByMin;
    private float receiptCurrentMonth;
    private float receiptLast30Days;
}
