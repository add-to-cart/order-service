package com.domosshop.orderservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Payment {
    private String status;
    private String gateway;
    private String type;
    private float amount;
    private String token;

    
}
