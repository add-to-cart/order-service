package com.domosshop.orderservice.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Customer {

    @Transient
	public static final String SEQUENCE_NAME = "customers_sequence";

    @Id
    private Long id;

    private String firstName;
    private String lastName;
    @Indexed(unique = true)
    private String email;
    private String password;
    private Payment payment;
    private Address address;
    private Address shippingAddress;
    
    public Customer(String firstName, String lastName, String email, String password, Payment payment, Address address,
            Address shippingAddress) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.payment = payment;
        this.address = address;
        this.shippingAddress = shippingAddress;
    }

    

}
