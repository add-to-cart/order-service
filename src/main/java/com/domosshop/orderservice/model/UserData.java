package com.domosshop.orderservice.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.springframework.data.mongodb.core.index.Indexed;

import com.domosshop.orderservice.annotation.constraint.Name;

import lombok.Data;

@Data
public class UserData {

    @NotBlank
    @Name
    private String firstName;
    
    @NotBlank
    @Name
    private String lastName;

    @NotBlank
    private String street1;
    private String street2;

    @NotBlank
    private String city;

    @Pattern(regexp = "^(([0-8][0-9])|(9[0-5]))[0-9]{3}$")
    private String zip;

    @NotBlank
    private String phone;

    @Email
    @Indexed(unique = true)
    private String email;

    private String orderNote;
}
