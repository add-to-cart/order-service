package com.domosshop.orderservice.model;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {

    @Transient
	public static final String SEQUENCE_NAME = "orders_sequence";
    
    @Id
    private Long id;
    private Long customerId;
    private Cart cart;
    private Address shipping;
    private LocalDateTime timestamp;
    private OrderStatus status;
    private float grandTotal;
    private String orderNote;

    public Order(Long customerId, Cart cart, Address shipping, LocalDateTime timestamp, OrderStatus status, float grandTotal, String orderNote) {
        this.customerId = customerId;
        this.cart = cart;
        this.shipping = shipping;
        this.timestamp = timestamp;
        this.status = status;
        this.grandTotal = grandTotal;
        this.orderNote = orderNote;
    }
    
}
