package com.domosshop.orderservice.model;

public enum OrderStatus {
    PENDING,
    CONFIRMED,
    REJECTED,
    CANCELED
}
