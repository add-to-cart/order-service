package com.domosshop.orderservice.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.domosshop.orderservice.model.Order;

@Repository
public interface OrderRepository
extends MongoRepository<Order, Long> {

    Optional<List<Order>> findAllByCustomerId(Long customerId);
    
}
