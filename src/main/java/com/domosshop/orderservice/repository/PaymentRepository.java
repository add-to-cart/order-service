package com.domosshop.orderservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.domosshop.orderservice.model.Payment;

public interface PaymentRepository extends MongoRepository<Payment, String>{
    
}
