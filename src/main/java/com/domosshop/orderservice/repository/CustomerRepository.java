package com.domosshop.orderservice.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.domosshop.orderservice.model.Customer;

@Repository
public interface CustomerRepository
extends MongoRepository<Customer, Long> {
    Optional<Customer> findCustomerByEmail(String email);
}
