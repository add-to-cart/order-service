package com.domosshop.orderservice.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    @Value(value = "${topic.order.detail}")
    private String detailOrderTopicName;

    @Value(value = "${topic.order.pending}")
    private String pendingOrderTopicName;

    @Value(value = "${topic.order.confirmed}")
    private String confirmedOrderTopicName;

    @Bean
    public NewTopic detailOrderTopic() {
        return TopicBuilder.name(detailOrderTopicName).build();
    }

    @Bean
    public NewTopic pendingOrderTopic() {
        return TopicBuilder.name(pendingOrderTopicName).build();
    }

    @Bean
    public NewTopic confirmedOrderTopic() {
        return TopicBuilder.name(confirmedOrderTopicName).build();
    }
}
