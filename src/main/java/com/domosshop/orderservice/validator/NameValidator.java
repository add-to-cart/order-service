package com.domosshop.orderservice.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.domosshop.orderservice.annotation.constraint.Name;

public class NameValidator implements ConstraintValidator<Name, String> {

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		Pattern pattern = Pattern
				.compile("^[A-ZÀ-ÿ]([A-Za-z0-9]+([\\\\,\\\\']{0,1}[ ]{0,1})|([\\\\-]{0,1}))+([A-Za-z0-9]+)$");
		Matcher matcher = pattern.matcher(value);
		try {
			if (!matcher.matches()) {
				return false;
			} else
				return true;
		} catch (Exception e) {
			return false;
		}
	}

}
