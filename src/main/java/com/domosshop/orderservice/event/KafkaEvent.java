package com.domosshop.orderservice.event;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.domosshop.orderservice.model.Address;
import com.domosshop.orderservice.model.Cart;
import com.domosshop.orderservice.model.Customer;
import com.domosshop.orderservice.model.ItemCart;
import com.domosshop.orderservice.model.JsonItemCart;
import com.domosshop.orderservice.model.Order;
import com.domosshop.orderservice.model.OrderJson;
import com.domosshop.orderservice.model.OrderStatus;
import com.domosshop.orderservice.model.Payment;
import com.domosshop.orderservice.model.UserData;
import com.domosshop.orderservice.service.CustomerService;
import com.domosshop.orderservice.service.OrderService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class KafkaEvent {

    @Autowired
    OrderService orderService;

    @Autowired
    CustomerService customerService;

    @Value(value = "${topic.order.pending}")
    private String pendingOrderTopicName;

    @Value(value = "${topic.order.confirmed}")
    private String confirmedOrderTopicName;

    private static final ObjectMapper oMapper = new ObjectMapper();

    @KafkaListener(topics = "detail_order", groupId = "order-service", containerFactory = "OrderRequestListenerContainerFactory")
    public void receiveOrderFromKafka(OrderJson order)
            throws JsonMappingException, JsonProcessingException, JSONException {
        JSONObject orderJson = new JSONObject(order);

        // get user data from orders
        UserData userData = oMapper.readValue(String.valueOf(orderJson.get("userData")), UserData.class);

        // get Shipping address of the user from orders
        Address shippingAddr = new Address(userData.getCity(), userData.getStreet1(), userData.getStreet2(),
                userData.getZip(), userData.getPhone());

        // get payment option of the user from orders
        Payment payment = new Payment("verified", "stripe", "credit", 10000, "tok_1LpeoQ2eZvKYlo2CSaq9bVe0");

        /**
         * Update the customer if email found in the DB
         * if not Add him
         */
        Customer customer = new Customer(userData.getFirstName(), userData.getLastName(), userData.getEmail(),
                "", payment, null, null);
        Customer customerAdded = new Customer();
        Customer customerFound = customerService.getByEmail(customer.getEmail());
        if (customerFound == null)
            customerAdded = customerService.add(customer);
        else {
            customer.setId(customerFound.getId());
            customerAdded = customerService.update(customer);
        }

        // get the cart json value from orders and save it in class Cart
        ArrayList<ItemCart> itemCarts = new ArrayList<>();
        JSONArray jsonItemCarts = orderJson.getJSONArray("carts");
        for (Object item : jsonItemCarts) {
            JsonItemCart jsonItemCart = oMapper.readValue(String.valueOf(item), JsonItemCart.class);
            itemCarts.add(new ItemCart(jsonItemCart.getId(), jsonItemCart.getQuantity(), jsonItemCart.getTotal()));
        }
        Cart cart = new Cart(itemCarts, LocalDateTime.now());

        // add the order in DB
        Order objOrder = new Order(customerAdded.getId(), cart, shippingAddr, LocalDateTime.now(),
                OrderStatus.PENDING, orderJson.getFloat("grandTotal"), userData.getOrderNote());
        Order orderAdded = orderService.addOrder(objOrder);

        log.info("\nKAFKA CONSUMER ON SPRING BOOT APP : ORDER-SERVICE" + order + "\n");

        orderService.sendOrderToKafka(orderAdded, pendingOrderTopicName);
    }

    @KafkaListener(topics = "pending_order", groupId = "order-service", containerFactory = "OrderRequestListenerContainerFactory")
    public void processConfirmedOrder(Order order) throws InterruptedException {

        log.info("\nKAFKA CONSUMER ON SPRING BOOT APP : processConfirmedOrder " + order + "\n");

        // simulate a payment process - time to interact with payment-service
        TimeUnit.SECONDS.sleep(30);

        if (order.getStatus() == OrderStatus.PENDING) {
            Customer customer = customerService.get(order.getCustomerId());
            var payment = customer.getPayment();
            payment.setAmount(payment.getAmount() - order.getGrandTotal());
            if (payment.getAmount() < 0)
                order.setStatus(OrderStatus.REJECTED);
            else {
                order.setStatus(OrderStatus.CONFIRMED);

                // update payment and user details of the user based on the order data
                customer.setPayment(payment);

                // check if the order is not canceled by the customer
                if (!orderService.getOrder(order.getId()).getStatus().equals(OrderStatus.CANCELED)) {
                    customerService.update(customer);

                    orderService.sendOrderToKafka(order, confirmedOrderTopicName);
                    orderService.updateOrder(order);
                }
            }
        }

        log.info("\nKAFKA CONSUMER ON SPRING BOOT APP : processConfirmedOrder end " + order + "\n");
    }
}
