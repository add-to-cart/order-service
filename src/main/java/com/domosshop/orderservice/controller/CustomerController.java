package com.domosshop.orderservice.controller;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.domosshop.orderservice.model.Customer;
import com.domosshop.orderservice.model.ResponseModel;
import com.domosshop.orderservice.service.CustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @GetMapping
    public ResponseEntity<ResponseModel> fetchAllCustomers() {
        try {
            var customers = customerService.getAllCustomers();
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.OK.value(), HttpStatus.OK.toString(),
                    "", "", "", customers));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.toString(),
                    "", "", e.getLocalizedMessage(), null));
        }
    }

    @PostMapping
    public ResponseEntity<ResponseModel> addCustomer(@RequestBody Customer customer) {
        try {
            Customer c = customerService.add(customer);

            return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.CREATED.value(), HttpStatus.CREATED.toString(),
                    "", "", "", c));

        } catch (Exception e) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.toString(),
                    "", "", "", null));
        }
    }

    @PutMapping
    public ResponseEntity<ResponseModel> updateCustomer(@RequestBody Customer customer) {
        try {
            Customer c = customerService.update(customer);

            return ResponseEntity.status(HttpStatus.OK).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.OK.value(), HttpStatus.OK.toString(),
                    "", "", "", c));

        } catch (Exception e) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.toString(),
                    "", "", "", null));
        }
    }

    @DeleteMapping("/{customerId}")
    public ResponseEntity<ResponseModel> deleteCustomer(@PathVariable("customerId") String id) {
        try {
            customerService.delete(customerService.get(Long.valueOf(id)));

            return ResponseEntity.status(HttpStatus.OK).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.OK.value(), HttpStatus.OK.toString(),
                    "", "", "", null));

        } catch (Exception e) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.toString(),
                    "", "", "", null));
        }
    }
}
