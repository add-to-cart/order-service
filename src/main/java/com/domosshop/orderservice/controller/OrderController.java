package com.domosshop.orderservice.controller;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.domosshop.orderservice.model.Customer;
import com.domosshop.orderservice.model.Order;
import com.domosshop.orderservice.model.OrderJson;
import com.domosshop.orderservice.model.OrderStat;
import com.domosshop.orderservice.model.OrderStatus;
import com.domosshop.orderservice.model.ResponseModel;
import com.domosshop.orderservice.service.CustomerService;
import com.domosshop.orderservice.service.OrderService;
import com.domosshop.orderservice.service.OrderStatService;

import lombok.extern.log4j.Log4j2;

@RestController
@RequestMapping("/order")
@Log4j2
public class OrderController {

    @Autowired
    OrderService orderService;

    @Autowired
    OrderStatService orderStatService;

    @Autowired
    CustomerService customerService;

    @Value(value = "${topic.order.detail}")
    private String detailOrderTopicName;

    @GetMapping
    public ResponseEntity<ResponseModel> fetchAllOrders() {
        try {
            List<Order> orders = orderService.getAllOrders();
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.OK.value(), HttpStatus.OK.toString(),
                    "", "", "", orders));
        } catch (Exception e) {
            log.info(e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.toString(),
                    "", "", e.getLocalizedMessage(), null));
        }
    }

    @PostMapping
    public ResponseEntity<ResponseModel> addOrder(@Valid @RequestBody OrderJson order) {
        try {

            /**
             * Send order to kafka topic "pending_order" to operate all validation
             * required like check if customer has enough money
             * and save the order to the database
             */
            orderService.sendOrderToKafka(order, detailOrderTopicName);

            return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.CREATED.value(), HttpStatus.CREATED.toString(),
                    "", "", "", order));

        } catch (Exception e) {
            log.info(e);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.toString(),
                    "", "", e.getLocalizedMessage(), order));
        }
    }

    /**
     * @description cancel or confirm an order based on user request and payment
     *              validation
     * @param order
     * @return
     */
    @PutMapping
    public ResponseEntity<ResponseModel> UpdateOrder(@Valid @RequestBody Order order) {
        try {
            if (order.getStatus() == OrderStatus.CONFIRMED) {
                Customer customer = customerService.get(order.getCustomerId());
                var payment = customer.getPayment();
                payment.setAmount(payment.getAmount() - order.getGrandTotal());
                if (payment.getAmount() < 0)
                    order.setStatus(OrderStatus.REJECTED);
                else {
                    customer.setPayment(payment);
                    customerService.update(customer);
                }
            }
            orderService.updateOrder(order);

            return ResponseEntity.status(HttpStatus.OK).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.OK.value(), HttpStatus.OK.toString(),
                    "", "", "", orderService.getAllOrders()));

        } catch (Exception e) {
            log.info(e);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.toString(),
                    "", "", e.getLocalizedMessage(), null));
        }
    }

    /**
     * @apiNote retrieve statistics about orders passed by all customers
     * @return {@link ResponseEntity<ResponseModel>}
     */
    @GetMapping("/stats")
    public ResponseEntity<ResponseModel> getOrderStats() {
        try {
            OrderStat orderStat = orderStatService.getOrderStat();

            return ResponseEntity.status(HttpStatus.OK).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.OK.value(), HttpStatus.OK.toString(),
                    "", "", "", orderStat));
        } catch (Exception e) {
            log.info(e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.toString(),
                    "", "", e.getLocalizedMessage(), null));
        }
    }

    @GetMapping("/customer/{customerEmail}")
    public ResponseEntity<ResponseModel> getOrdersByCustomerEmail(@PathVariable("customerEmail") String customerEmail) {
        try {
            List<Order> orders = orderService.getOrdersByCustomerEmail(customerEmail);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.OK.value(), HttpStatus.OK.toString(),
                    "", "", "", orders));
        } catch (Exception e) {
            log.info(e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.toString(),
                    "", "", e.getLocalizedMessage(), customerEmail));
        }
    }

    @DeleteMapping("/{orderId}")
    public ResponseEntity<ResponseModel> deleteOrder(@PathVariable("orderId") String id) {
        try {
            orderService.delete(orderService.getOrder(Long.valueOf(id)));

            return ResponseEntity.status(HttpStatus.OK).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.OK.value(), HttpStatus.OK.toString(),
                    "", "", "", null));

        } catch (Exception e) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseModel(LocalDateTime.now(),
                    HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.toString(),
                    "", "", e.getLocalizedMessage(), null));
        }
    }
}
