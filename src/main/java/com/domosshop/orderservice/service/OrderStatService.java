package com.domosshop.orderservice.service;

import com.domosshop.orderservice.model.OrderStat;

public interface OrderStatService {
    
    public OrderStat getOrderStat();
}
