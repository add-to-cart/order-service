package com.domosshop.orderservice.service;

import java.util.List;

import javax.validation.Valid;

import com.domosshop.orderservice.model.Order;

public interface OrderService {
    public List<Order> getAllOrders();
    public Order getOrder(Long id);
    public Order addOrder(@Valid Order order);
    public Order updateOrder(@Valid Order order);
    public void delete(Order order);

    public void sendOrderToKafka(Object order, String topic);

    public List<Order> getOrdersByCustomerEmail(String customerEmail);
}
