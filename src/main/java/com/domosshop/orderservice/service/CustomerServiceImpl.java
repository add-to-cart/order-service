package com.domosshop.orderservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domosshop.orderservice.model.Customer;
import com.domosshop.orderservice.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepo;

    @Autowired
	SequenceGenerator sequenceGenerator;


    @Override
    public Customer add(Customer c) {
        // generate a id type Long
        c.setId(sequenceGenerator.generateSequence(Customer.SEQUENCE_NAME));
        return customerRepo.insert(c);
    }

    @Override
    public Customer update(Customer c) {
        return customerRepo.save(c);
    }

    @Override
    public Customer get(Long id) {
        return customerRepo.findById(id).orElse(null);
    }

    @Override
    public Customer getByEmail(String email) {

        return customerRepo.findCustomerByEmail(email).orElse(null);
    }

    @Override
    public void delete(Customer c) {
        customerRepo.delete(c);
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepo.findAll();
    }

}
