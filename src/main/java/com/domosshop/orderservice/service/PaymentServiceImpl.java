package com.domosshop.orderservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.domosshop.orderservice.model.Payment;
import com.domosshop.orderservice.repository.PaymentRepository;

public class PaymentServiceImpl implements PaymentService{

    @Autowired
	SequenceGenerator sequenceGenerator;

    @Autowired
    PaymentRepository paymentRepository;

    @Override
    public Payment add(Payment p) {
        // p.setId(sequenceGenerator.generateSequence(Payment.SEQUENCE_NAME));
        return paymentRepository.insert(p);
    }

    @Override
    public Payment update(Payment p) {
        return paymentRepository.save(p);
    }

    @Override
    public Payment get(String id) {
        return paymentRepository.findById(id).orElse(null);
    }

    @Override
    public void delete(Payment p) {
        paymentRepository.delete(p);
        
    }

    @Override
    public List<Payment> getAllPayments() {
        return paymentRepository.findAll();
    }
    
}
