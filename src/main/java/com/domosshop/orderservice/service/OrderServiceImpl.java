package com.domosshop.orderservice.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.validation.annotation.Validated;

import com.domosshop.orderservice.model.Order;
import com.domosshop.orderservice.repository.OrderRepository;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
@Validated
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    CustomerService customerService;

    @Autowired
    SequenceGenerator sequenceGenerator;

    @Autowired
    KafkaTemplate<String, Object> kafkaTemplate;

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @Override
    public Order getOrder(Long id) {
        return orderRepository.findById(id).orElse(null);
    }

    @Override
    public Order addOrder(@Valid Order order) {
        // generate a id type Long
        order.setId(sequenceGenerator.generateSequence(Order.SEQUENCE_NAME));
        return orderRepository.insert(order);
    }

    @Override
    public Order updateOrder(@Valid Order order) {
        return orderRepository.save(order);
    }

    @Override
    public void delete(Order order) {
        orderRepository.delete(order);
    }

    @Override
    public void sendOrderToKafka(Object order, String topic) {
        ListenableFuture<SendResult<String, Object>> future = kafkaTemplate.send(topic, order);

        future.addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {

            @Override
            public void onSuccess(SendResult<String, Object> result) {
                System.out.println("Sent message=[" + order + 
                  "] with offset=[" + result.getRecordMetadata().offset() + "]");
            }
            @Override
            public void onFailure(Throwable ex) {
                System.out.println("Unable to send message=[" 
                  + order + "] due to : " + ex.getMessage());
            }
        });
        
        log.debug("ORDER SEND TO KAFKA !!!");
    }

    @Override
    public List<Order> getOrdersByCustomerEmail(String customerEmail) {
        Long customerId = customerService.getByEmail(customerEmail).getId();
        return orderRepository.findAllByCustomerId(customerId).orElse(null);
    }
}
