package com.domosshop.orderservice.service;

import java.util.List;

import com.domosshop.orderservice.model.Customer;

public interface CustomerService {
    public Customer add(Customer c);
    public Customer update(Customer c);
    public Customer get(Long id);
    public Customer getByEmail(String email);
    public void delete(Customer c);
    public List<Customer> getAllCustomers();
}
