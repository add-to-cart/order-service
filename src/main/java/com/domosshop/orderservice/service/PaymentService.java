package com.domosshop.orderservice.service;

import java.util.List;

import com.domosshop.orderservice.model.Payment;

public interface PaymentService {
    public Payment add(Payment c);
    public Payment update(Payment c);
    public Payment get(String id);
    public void delete(Payment c);
    public List<Payment> getAllPayments();
    
}
