package com.domosshop.orderservice.service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.domosshop.orderservice.model.Order;
import com.domosshop.orderservice.model.OrderStat;
import com.domosshop.orderservice.model.OrderStatus;
import com.domosshop.orderservice.repository.OrderRepository;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class OrderStatServiceImpl implements OrderStatService {

    @Autowired
    OrderRepository orderRepository;

    @Override
    public OrderStat getOrderStat() {

        OrderStat orderStat = new OrderStat();

        var orders = orderRepository.findAll();

        if (orders.size() == 0)
            return orderStat;

        // calculate the number of orders rejected
        var nbrOrderReject = orders.stream().filter(order -> order.getStatus().equals(OrderStatus.REJECTED)).count();
        orderStat.setNbrOrderRejected((int) nbrOrderReject);

        // calculate the number of orders canceled
        var nbrOrderCanceled = orders.stream().filter(order -> order.getStatus().equals(OrderStatus.CANCELED)).count();
        orderStat.setNbrOrderCanceled((int) nbrOrderCanceled);

        // calculate total receipt for the last 30 days
        orders.stream()
                .filter(order -> order.getStatus().equals(OrderStatus.CONFIRMED) &&
                        order.getTimestamp().plusDays(30).isAfter(LocalDateTime.now()))
                .forEach(order -> {
                    orderStat.setReceiptLast30Days(orderStat.getReceiptLast30Days() + order.getGrandTotal());
                });

        // calculate total receipt for the current month
        orders.stream()
                .filter(order -> order.getStatus().equals(OrderStatus.CONFIRMED) &&
                        order.getTimestamp().getMonth().equals(LocalDateTime.now().getMonth()))
                .forEach(order -> {
                    orderStat.setReceiptCurrentMonth(orderStat.getReceiptCurrentMonth() + order.getGrandTotal());
                });

        // calculate the receipt by hour and min
        float totalReceipt = 0;

        var streamOrdersConfirmedSorted = orders.stream()
                .filter(order -> order.getStatus().equals(OrderStatus.CONFIRMED))
                .sorted(Comparator.comparing(Order::getTimestamp));

        var ordersConfirmedSorted = streamOrdersConfirmedSorted.toList();

        // s'il y a aucune commande confirmée, l'argent percu sera à zéro dans tous les
        // cas suivants
        if (ordersConfirmedSorted.size() != 0) {

            // datetime for the first confirmed order
            LocalDateTime fromDateTime = ordersConfirmedSorted.get(0).getTimestamp();

            // period between the first confirmed order and the last confirmed order
            long mins = fromDateTime.until(
                    LocalDateTime.now(), ChronoUnit.MINUTES);
            float hours = (float)mins / 60;

            // total receipt between the first confirmed order and the last confirmed order
            for (Order order : ordersConfirmedSorted) {
                totalReceipt += order.getGrandTotal();
            }
            // calculate the receipt by hour 
            if (hours >= 1 && ordersConfirmedSorted.size() > 1)
                orderStat.setReceiptByHour(totalReceipt / hours);
            else
                orderStat.setReceiptByHour(totalReceipt);

            // calculate receipt by min
            if (mins >= 1 && ordersConfirmedSorted.size() > 1)
                orderStat.setReceiptByMin(totalReceipt / mins);
            else
                orderStat.setReceiptByMin(totalReceipt);
        }

        return orderStat;
    }

}
