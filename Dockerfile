FROM openjdk:slim

RUN addgroup --system spring && adduser --system spring --group
USER spring:spring

ARG TAG
ARG APP_NAME

COPY target/$APP_NAME-$TAG.jar app.jar

ENTRYPOINT ["java","-jar","/app.jar"]