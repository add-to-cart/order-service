terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}

provider "docker" {}

resource "docker_image" "mongodb" {
  name         = "mongo:focal"
  keep_locally = false
}

resource "docker_container" "mongodb" {
  image = docker_image.mongodb.latest
  name  = "mongodb"
  ports {
    internal = 27017
    external = 27017
  }
  
}

resource "docker_image" "order_service" {
  name         = "wi11i4m/order-service:latest"
  keep_locally = false
}

resource "docker_container" "order_service" {
  image = docker_image.order_service.latest
  name  = "order-service"
  ports {
    internal = 5000
    external = 5000
  }
}